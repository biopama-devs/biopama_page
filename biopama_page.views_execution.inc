<?php

/**
 * @file
 * Provides views data and hooks for biopama_dashboards module.
 */
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 */
function biopama_page_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
    $view_id = $view->storage->id();
    switch ($view_id) {
      case 'page_resources':
      case 'pame_resources':
        $query->addField('node_field_data', 'nid', '', ['function' => 'groupby']);
        $query->addGroupBy('node_field_data.nid');
        break;
      default:
        return FALSE;
    }
} 