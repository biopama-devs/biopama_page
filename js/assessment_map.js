/**
 * This component handles the generation of an assessment map (e.g., as in the /page_assessments module). It exposes the following api
 *  
 * - initMap: the function creating the whole map. A callback can be optinally passed and it will be eventually called after the map is fully loaded.
 * - showFeatureById: allows to show (pan+zoom) to the element identified by the passed (wdpa-) id.
 * - showFeaturesByIdAndBbox: allows to show to the elements identified by the passed (wdpa-) ids and with a viewport able to contain the passed bbox.
 */

var getFeatureInfoUrl = "/rest/gd_page";
var biopmaApiBaseUrl = "https://api.biopama.org/api/pame/function";

var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	ISO2: null,
	ISO3: null,
	NUM: null,
};

var biopamaAssessmentMap;

(function($){
	
	var map;
	function initMap(onceMapLoadedcallback){
		map = $().createMap("biopama_map");
		$().addMapLayerBiopamaSources(map);
		$().addMapLayer(map, "biopamaWDPAPolyJRC");
		map.on('click', getFeatureInfo);
		map.setMinZoom(3);
		
		if(onceMapLoadedcallback){
			$.fn.attachMapEventHandler(map, 'load', onceMapLoadedcallback);
		}
	}

    function getFeatureInfo(e){
		var features = $.fn.getSelectedFeatures(map, e.point, ["wdpaAcpMask"]);

		console.log("Features selected: "+features.length);
		if(features.length > 0){
			selSettings.WDPAID = features[0].properties.wdpaid
			$.getJSON(
				getFeatureInfoUrl+'?format=json&wdpaid='+selSettings.WDPAID,
				function(result){
					var data = result, paPopupContent;
					if(data.length > 0){
						paPopupContent = '<center class="available text-black"><i class="fas fa-2x fa-paste"></i><p>'+
						data[0].pa.replace('<a ', '<a target="_blank" ')+' ('+data[0].wdpa_id+')</p>';
						for(var key in data){
							paPopupContent += '<i>'+data[key].ass_method+' ('+data[key].ass_year+')</i><hr>';
						}
						paPopupContent += '</center>';
						
					}
					else{
						paPopupContent  = '<center class="available"><i class="fas fa-2x fa-paste"></i><p>No results found to be displayed.</p></center>';
					}
					$.fn.createMapPopup(map, e.lngLat, paPopupContent);
				}
			);
		}
	}

	function showFeatureById(wdpaId){
		$().mapZoomToPA(map, wdpaId);
		$.fn.setMapFilter(map, "wdpaAcpSelected", ['==', 'wdpaid', wdpaId]);
		$.fn.setLayerVisibility(map, "wdpaAcpSelected", true);
	}

	function showFeaturesByIdAndBbox(wdpaIds, bbox){
		// Initialize the variables used to display the right layer and the right viewport on the map.
		var assessmentsByWDPA = ['in', 'wdpaid'].concat(wdpaIds);
		$.fn.setMapFilter(map, "wdpaAcpSelected", assessmentsByWDPA);
		$.fn.setLayerVisibility(map, "wdpaAcpSelected", true);

		if(bbox){
			$.fn.setMapBounds(map, bbox);
		}
	}

	biopamaAssessmentMap = {
		initMap: initMap,
		showFeatureById: showFeatureById,
		showFeaturesByIdAndBbox: showFeaturesByIdAndBbox
	}
})(jQuery);