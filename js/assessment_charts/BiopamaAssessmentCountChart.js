class BiopamaAssessmentCountChart extends BiopamaAssessmentChart{
	constructor(elementSelector, itemOptions){
		super(elementSelector, itemOptions, 'count');
		this.createChart();
        this.setupEventListener();
		if(Array.isArray(this.options.colors)){
			this.options.colors = this.options.colors[0];
		}
	}

	createChart(){
        this.getData(
            this,
            function(obj, responseData){
                let $domElements = obj.prepareDomElements();

                if(!obj.chart){
					$domElements.body.addClass('biopama-stats-item-text');
					obj.chart = $domElements.item;
				}
				else{
					$domElements.body = jQuery('.biopama-stats-item-content', obj.domElement);
				}
				
				$domElements.body.html(obj.options.dataMapping(responseData));
            }
        );
    }

    update(params){
        this.getData(
            this,
            function(obj, data){
                jQuery('.biopama-stats-item-content', obj.domElement).text(obj.options.dataMapping(responseData).value);
            },
            params
        );
    }
}