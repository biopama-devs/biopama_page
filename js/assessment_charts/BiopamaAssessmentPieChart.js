class BiopamaAssessmentPieChart extends BiopamaAssessmentChart{
	constructor(elementSelector, itemOptions){
		super(elementSelector, itemOptions, 'pie');
		this.createChart();
        this.setupEventListener();
	}

	createChart(){
        let chartOptions;
        this.getData(
            this,
            function(obj, responseData){
                if(!obj.chart){
					let $domElements = obj.prepareDomElements();
					$domElements.body.addClass('biopama-stats-item-chart');
				
					chartOptions = {
						color: obj.options.colors,
						tooltip: {
							trigger: 'item'
						},
						series: obj.getSeries([
							{
								type: obj.chartType,
								radius: obj.options.radius || '60%',
								data: obj.options.dataMapping(responseData),
								emphasis: {
									itemStyle: {
										shadowBlur: 10,
										shadowOffsetX: 0,
										shadowColor: 'rgba(0, 0, 0, 0.5)'
									}
								},
								label: {
									color: obj.options.labelColor
								}
							}
						])
					};
					obj.chart = jQuery.fn.createNoAxisChart($domElements.body.get(0), chartOptions);
				}
            }
        );
    }

    update(params){
        this.getData(
            this,
            function(obj, data){
                obj.chart.setOption({
					series: [{
						data: obj.options.dataMapping(data)
					}]
				});
				obj.resize();
            },
            params
        );
    }
}