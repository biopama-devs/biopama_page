class BiopamaAssessmentChart{
    constructor(domElementOrSelector, chartOptions, chartType){
        if(typeof domElementOrSelector == 'string'){
            this.domElement = document.querySelector(domElementOrSelector);
        }
        else{
            this.domElement = domElementOrSelector;
        }
        this.chartType = chartType;
        this.options = chartOptions;

        this.options.titleColor = this.options.titleColor || biopamaGlobal.chart.colors.text;
        this.options.labelColor = this.options.labelColor || biopamaGlobal.chart.colors.text;
        this.options.color = this.options.colors || biopamaPAColors.terrestrial;
        this.options.queryStringSeparator = this.options.queryStringSeparator || ',';
        this.options.queryStringParamsSeparator = this.options.queryStringParamsSeparator || ',';
        //delete this.options.color;
        // if(chartOptions.colors){
        //     this.options.colors = biopamaGlobal.chart.colors[chartOptions.colors] || chartOptions.colors;
        // }
        // console.log(this)
        if(this.options.series){
            this.options.series.type = this.chartType;
        }
    }

    resize(){
        if(this.chart && this.chart.resize){
            this.chart.resize();
        }
    }

    // getTitle(){
    //     return {
    //         text: this.options.customTitle || this.options.title,
    //         show: (this.options.customTitle? false : true)
    //     };
    // }

    getSeries(defaultSeries){
        return this.options.series || defaultSeries;
    }

    getData(obj, callback, otherParams){
        let finalUrl = this.options.url;
        let params = otherParams || this.options.params;
        if(params){
            let urlParams = [];
            for(let k in params){
                urlParams.push(`${k}=${params[k]}`);
            }
            finalUrl += `${this.options.queryStringSeparator}${urlParams.join(this.options.queryStringParamsSeparator)}`;
        }
        jQuery.ajax({
            type: 'GET',
            dataType: 'json',
            url: finalUrl,
            success: function(data, status, xhr){
                if(callback){
                    callback(obj, data);
                }
            },
            error: function(xhr, status, error){
                console.error(`${status} - ${error}`);
            }
        });
    }

    setupEventListener(){
        if(this.options.updateEvent){
            let obj = this;
            jQuery(document).on(
                this.options.updateEvent,
                function(e, data){
                    let newData = data;
                    if(obj.options.updateEventDataSetup){
                        newData = obj.options.updateEventDataSetup(data);
                    }
                    // console.log(JSON.stringify(newData));
                    obj.update(newData);
                }
            );
        }
    }

    prepareDomElements(){
        let $item = jQuery(`<div class="biopama-stats-item"></div>`);
                
        let $itemTitle = jQuery(`<h5>${this.options.customTitle? this.options.customTitle : ''}</h5>`);
        let $itemBody = jQuery(`<div class="biopama-stats-item-content"></div>`);

        jQuery(this.domElement).append($item);

        if(!this.chart){
            $item.append($itemTitle);
            $item.append($itemBody);
        }

        return {
            item: $item,
            title: $itemTitle,
            body: $itemBody
        };
    }
}