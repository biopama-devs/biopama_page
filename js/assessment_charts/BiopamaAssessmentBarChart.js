class BiopamaAssessmentBarChart extends BiopamaAssessmentChart{
	constructor(elementSelector, itemOptions){
		super(elementSelector, itemOptions, 'bar');
		this.createChart();
        this.setupEventListener();
	}

	createChart(){
        let chartOptions;
        this.getData(
            this,
            function(obj, responseData){
				if(!obj.chart){
                	let $domElements = obj.prepareDomElements();
					$domElements.body.addClass('biopama-stats-item-chart');
					
					chartOptions = {
						color: obj.options.color,
						tooltip: {
							trigger: 'axis'
						},
						toolbox: {
							show: true,
							right: 20,
							top: -8,
							feature: {
								saveAsImage: {
									title: 'Save',
								}
							}
						},
						xAxis: {
							type: 'category',
							data: obj.options.valuesX(responseData),
							axisLabel: {
								color: obj.options.labelColor
							}
						},
						yAxis: {
							type: 'value',
							axisLabel: {
								color: obj.options.labelColor
							}
						},
						dataZoom: [
							{
								type: 'slider',
								xAxisIndex: [0],
								filterMode: 'filter'
							}
						],
						series: obj.getSeries([
							{
								type: obj.chartType,
								data: obj.options.dataMapping(responseData),
								emphasis: {
									itemStyle: {
										shadowBlur: 10,
										shadowOffsetX: 0,
										shadowColor: 'rgba(0, 0, 0, 0.5)'
									}
								},

							}
						])
					};

					obj.chart = jQuery.fn.createXYAxisChart($domElements.body.get(0), chartOptions);
				}
            }
        );
    }

    update(params){
        this.getData(
            this,
            function(obj, data){
                obj.chart.setOption({
					xAxis: {
						data: obj.options.valuesX(data)
					}
				});
				obj.chart.setOption({
					series: [{
						data: obj.options.dataMapping(data)
					}]
				});
				obj.resize();
            },
            params
        );
    }
}