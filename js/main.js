(function($){
  var fields = {
    "wdpa_id": {
      name: "WDPA ID",
      get_value: function(json_response_data){
        let $obj = jQuery(`<a href="/ct/pa/${json_response_data['wdpa_id']}">${json_response_data['wdpa_id']}</a>`);
        return $obj;
      }
    },
    "region": {
      name: "Region",
      removable: true,
      filter: {
        display_order: 3,
        label: "",
        default_option: "All Regions",
        // default_value: "%",
        default_value: biopamaAssessmentTable.getRegions('array').join(','),
        get_value: function(json_response_data){
          return $(json_response_data["region"]).length > 0? $(json_response_data["region"]).text() : json_response_data["region"];
        },
        get_text: function(json_response_data){
          return $(json_response_data["region"]).length > 0? $(json_response_data["region"]).text() : json_response_data["region"];
        }
      }
    },
    "iso3": {
      name: "ISO3",
      filter: {
        display_order: 4,
        label: "",
        default_option: "All Countries",
        default_value: "%",
        get_text: function(json_response_data){
          return $(json_response_data["country"]).text();
        }
      }
    },
    "country": {
      name: "Country",
    },
    "pa": {
      name: "Protected Area",
    },
    "designation": {
      name: "Designation",
      get_value: function(json_response_data){
        let $obj = jQuery(json_response_data['designation']);
        return $obj.html();
      }
    },
    "ass_method": {
      name: "Methodology",
      filter: {
          display_order: 1,
          label: "",
          default_option: "All Methodologies",
          default_value: "%"
      }
    },
    "ass_year": {
      name: "Year",
      filter: {
          display_order: 2,
          label: "",
          default_option: "All Years",
          default_value: "%"
      }
    },
    "assessment_report": {
      name: "Report"
    },
    "ass_link": {
      name: "Link",
    },
    "source": {
      name: "Source",
      filter: {
          display_order: 6,
          label: "",
          default_option: "All Sources",
          default_value: "%"
      }
    },
    "edit_link": {
      name: "Edit",
      removable: true
    },
  };

  biopamaAssessmentMap.initMap(
    function(){
      biopamaAssessmentTable.initAssessmentTable('/rest/gd_page', fields, biopamaAssessmentMap);
    }
  );

  Drupal.behaviors.PAGEFormTitleChange = {
    attach: function(context, settings){
      $(document).ready(function(){
        var $element = $(".ui-dialog-title").first();
        $element.text($element.text().replace('Create', 'Add'));
      });

		  $('#drupal-off-canvas').find('form.node-gd-page-assessment-form .alert-success, form.node-gd-page-assessment-edit-form .alert-success').once('updated-view').each( function() {
        $("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay(800).trigger('click');
        biopamaAssessmentTable.setTableData();
	  	});
    }
  };

  url = '/rest/gd_page_assessments_count_total';
	$.getJSON( url, function(d) {
	  var bigValue = d[0]['acp_count'];
	  var indicatorTitle = 'Total number of assessments'
	  var chartData = {
      title: indicatorTitle, 
      bigNumber: bigValue,
	  }
	  $.fn.createCountChart('#stats-total-assessments-count-acp', chartData);
	});

  let assessmentsCountByRegionChart = new BiopamaAssessmentPieChart(
    '#stats-assessments-count-by-region',
    {
      customTitle: 'Assessments by region',
      titleColor: biopamaGlobal.chart.colors.text,
      labelColor: biopamaGlobal.chart.colors.text,
      url: '/rest/gd_page_assessments_count_by_region',
      params: {
        region: biopamaAssessmentTable.getRegions('array').join(',')
      },
      queryStringSeparator: '?',
      queryStringParamsSeparator: '&',
      updateEvent: 'stats-data-changed',
      size: '40%',
      updateEventDataSetup: function(data){
        let newData = JSON.parse(JSON.stringify(data));
        delete newData.wdpaid;
        delete newData.source;
        for(let k in newData){
          if(!newData[k]){
            newData[k] = '';
          }
        }
        return newData;
      },
      dataMapping: function(responseData){
        let itemData = [];
        responseData.forEach(rd => {
          itemData.push({
            name: rd['group_type'],
            value: Number(rd['assessments_count'])
          });
        });
        return itemData;
      },
    },
  );

  let assessmentsCountByCountryChart = new BiopamaAssessmentBarChart(
    '#stats-assessments-count-by-country',
    {
      customTitle: 'Assessments by country',
      colors: [biopamaPAColors.terrestrial],
      titleColor: biopamaGlobal.chart.colors.text,
      labelColor: biopamaGlobal.chart.colors.text,
      url: '/rest/gd_page_assessments_count_by_country',
      params: {
        region: biopamaAssessmentTable.getRegions('array').join(',')
      },
      queryStringSeparator: '?',
      queryStringParamsSeparator: '&',
      updateEvent: 'stats-data-changed',
      updateEventDataSetup: function(data){
        let newData = JSON.parse(JSON.stringify(data));
        delete newData.wdpaid;
        delete newData.source;
        for(let k in newData){
          if(!newData[k]){
            newData[k] = '';
          }
        }
        return newData;
      },
      valuesX: function(responseData){
        responseData.sort((a, b) => {
          return (Number(a.assessments_count) < Number(b.assessments_count)? 1 : -1);
        });
        let xAxisValues = responseData.map(function(rd){
          return rd.group_type;
        });
        return xAxisValues;
      },
      dataMapping: function(responseData){
        responseData.sort((a, b) => {
          return (Number(a.assessments_count) < Number(b.assessments_count)? 1 : -1);
        });
        let itemData = responseData.map(function(rd){
          return Number(rd.assessments_count);
        });
        return itemData;
      }
    }
  );

  let assessmentsCountByYearChart = new BiopamaAssessmentBarChart(
    '#stats-assessments-count-by-year',
    {
      customTitle: 'Assessments by year',
      colors: [biopamaPAColors.terrestrial],
      titleColor: biopamaGlobal.chart.colors.text,
      labelColor: biopamaGlobal.chart.colors.text,
      url: '/rest/gd_page_assessments_count_by_year',
      params: {
        region: biopamaAssessmentTable.getRegions('array').join(',')
      },
      queryStringSeparator: '?',
      queryStringParamsSeparator: '&',
      updateEvent: 'stats-data-changed',
      updateEventDataSetup: function(data){
        let newData = JSON.parse(JSON.stringify(data));
        delete newData.wdpaid;
        delete newData.source;
        for(let k in newData){
          if(!newData[k]){
            newData[k] = '';
          }
        }
        return newData;
      },
      valuesX: function(responseData){
        let xAxisValues = responseData.map(function(rd){
          return rd.group_type;
        });
        return xAxisValues;
      },
      dataMapping: function(responseData){
        let itemData = responseData.map(function(rd){
          return rd.assessments_count;
        });
        return itemData;
      }
    }
  );
  

  let assessmentsCountByMethodologyChart = new BiopamaAssessmentBarChart(
    '#stats-assessments-count-by-methodology',
    {
      customTitle: 'Assessments by methodology',
      colors: [biopamaPAColors.terrestrial],
      url: '/rest/gd_page_assessments_count_by_methodology',
      params: {
        region: biopamaAssessmentTable.getRegions('array').join(',')
      },
      queryStringSeparator: '?',
      queryStringParamsSeparator: '&',
      updateEvent: 'stats-data-changed',
      updateEventDataSetup: function(data){
        let newData = JSON.parse(JSON.stringify(data));
        delete newData.wdpaid;
        delete newData.source;
        for(let k in newData){
          if(!newData[k]){
            newData[k] = '';
          }
        }
        return newData;
      },
      valuesX: function(responseData){
        responseData.sort((a, b) => {
          return (Number(a.assessments_count) < Number(b.assessments_count)? 1 : -1);
        });
        let xAxisValues = responseData.map(function(rd){
          return rd.group_type;
        });
        return xAxisValues;
      },
      dataMapping: function(responseData){
        responseData.sort((a, b) => {
          return (Number(a.assessments_count) < Number(b.assessments_count)? 1 : -1);
        });
        let itemData = responseData.map(function(rd){
          return rd.assessments_count;
        });
        return itemData;
      }
    }
  );
})(jQuery);