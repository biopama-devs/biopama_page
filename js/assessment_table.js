/**
 * This component handles the generation of an assessment table (e.g., as in the /page_assessments module) to make it automatically generated, passing the columns structure 
 * through a json object which should contain innerly as many other objects as the columns of the desrired table.
 * Each of such objects should specify:
 * - Key: the id of the coulmn within the table; such id should be equal to the name of the field in the response object from the service called in order to populate the table.
 * - Object's properties:
 * 		- name: The name that will be displayed as column header in the table.
 * 		- filter: a json OPTIONAL defining the properties of the (optional) filter to be applied to the column
 * 			- display_order: a (optional) number defining the order the placement of the filter within the filters' list.
 * 			- label: The text that will be displayed besides the filter.
 * 			- default_option: The default selection item's text
 * 			- default_value: The default selection item's value ('%' for no choice)
 * 			- get_text: A function, having as argument the service's response data, that can be used in order to give the selection item a dynamic text.
 * 			- get_value: A function, having as argument the service's response data, that can be used in order to give the selection item a dynamic value.
 * 
 * The generation of the table (which exploits the DataTable.js library) can be started by invoking the exposed initAssessmentTable function.
 */

var biopamaAssessmentTable;

(function($){
	var table;
	var filters = {};
	var assessmentMap;
	var tableDataServiceUrl;
	var allowedRegions = getRegions('array');

	function setupClearButton(){
		$('#reset_filters').on('click',function(){
			var currentURL = document.location.href;
			if(currentURL.includes('?')) {
				currentURL = currentURL.split('?')[0];
				history.pushState({}, null, currentURL);
			}

			for(k in filters){
				if(k == 'region'){
					filters[k] = allowedRegions.join(',');
				}
				else{
					filters[k] = null;
				}
			}
			
			setTableData();
			// setChartData();
			jQuery(document).trigger('stats-data-changed', filters);
		});
	}

	function setupFilterChangeEvent(){
		$('.cql_filters').on(
			'change',
			function(){
				var currentURL = document.location.href;
				var parameter = $(this).attr('param');
				var value = $(this).val();
				if(currentURL.includes(parameter)) {
					currentURL = removeURLParameter(currentURL, parameter);
				}
				if(value == '%'){
					filters[parameter] = null;
				}
				else{
					filters[parameter] = value;
					qsAddSymbol = currentURL.includes('?')? '&' : '?';
					currentURL = `${currentURL}${qsAddSymbol}${parameter}=${value}`;
				}
				history.pushState({}, null, currentURL);
				setTableData();
				// setChartData();
				jQuery(document).trigger('stats-data-changed', filters);
			}
		);
	}
	
	function setupFilters(){
		var $filterContainer = $("#biopama_filter");
		var $filterLabel, $filterItem, $filterItemDefaultOption, sortedFilterItems = [];

		var sortFilterItems = function($f1, $f2){
			var order = 0;
			var f1do = Number($f1.data('displayOrder'));
			var f2do = Number($f2.data('displayOrder'));
			if(f1do < f2do){
				order = -1;
			}
			else if(f1do > f2do){
				order = 1;
			}
			return order;
		}

		var itemOrder;
		for(k in response_fields_to_table_map){
			if(response_fields_to_table_map[k].filter){
				itemOrder = response_fields_to_table_map[k].filter.display_order? response_fields_to_table_map[k].filter.display_order : 1000;

				$filterLabel = $(`<label data-display-order="${itemOrder}" for="${k}">${response_fields_to_table_map[k].filter.label}</label>`);
				sortedFilterItems.push($filterLabel);

				$filterItem = $(`<select data-display-order="${itemOrder}" class="form-control cql_filters" id="${k}" param="${k}" tabindex="-1">`);
				$filterItemDefaultOption = $(`<option value="${response_fields_to_table_map[k].filter.default_value}">${response_fields_to_table_map[k].filter.default_option}</option>`);
				$filterItem.append($filterItemDefaultOption);
				sortedFilterItems.push($filterItem);

				filters[k] = null;
			}
		}
		sortedFilterItems.sort(sortFilterItems);
		$filterContainer.append(sortedFilterItems);
		$filterContainer.append($('<button class="btn btn-info" type="button" id="reset_filters">Clear All</button>'));

		var url_string = new URL(window.location.href);
		for(k in filters){
			if(url_string.searchParams.get(k)){
				filters[k] = url_string.searchParams.get(k);
			}
		}

		setupClearButton();
		setupFilterChangeEvent();
	}

	function updateFilters(data){
		$.each($('.cql_filters'),function(){
			$(this).find('option').not(':first').remove();
		});

		var generalFilterList = {}, option_value, option_text;
		data.forEach((d, i) => {
			for(k in d){
				if(response_fields_to_table_map[k] && response_fields_to_table_map[k].filter){
					option_value = (response_fields_to_table_map[k].filter.get_value)? response_fields_to_table_map[k].filter.get_value(d) : d[k];
					option_text = (response_fields_to_table_map[k].filter.get_text)? response_fields_to_table_map[k].filter.get_text(d) : d[k];
					if(option_value && option_text){
						if(!generalFilterList[k]){
							generalFilterList[k] = {};
						}
						generalFilterList[k][option_text] = {value: option_value};
					}
				}
			}
		});

		var generalFilterList, sortedKey, valueObj;
		for(k in generalFilterList){
			generalFilterListSortedKeys = Object.keys(generalFilterList[k]).sort();
			for(i in generalFilterListSortedKeys){
				sortedKey = generalFilterListSortedKeys[i];
				valueObj = generalFilterList[k][sortedKey];
				$(`#${k}.cql_filters`).append($(`<option value="${valueObj.value}">${sortedKey}</option>`));
			}

			if(filters[k]){
				$(`#${k}.cql_filters`).val(filters[k]);
			}
		}
	}

	function retrieveTableColumns(fields){
		var columns = [];
		for(k in fields){
			columns.push({
				data: k
			});
		}
		return columns;
	}

	function generateRestArgs(){
		var cleanArgs = '';
		for(var propName in filters){
			if((filters[propName] != null) || (filters[propName] != undefined)){
				if(propName != 'region' || (propName == 'region' && allowedRegions.includes(filters[propName]))){
					cleanArgs += '&' + propName + '=' + filters[propName];
				}
			}
			else if(propName == 'region'){
				cleanArgs += '&' + propName + '=' + getRegions('array').join(',');
			}
		}
		if(!cleanArgs){
			cleanArgs += '&' + 'region=' + getRegions('array').join(',');
		}
		return cleanArgs;
	}

	function getRegions(type = "string", removeACP = true, seperator = ","){
		var regions = biopamaGlobal.regions
		var regionStr = '';
		var regionArr = []; 
		if (removeACP){ //this will remove ACP from the regions
			regions = regions.filter(function( obj ) {
				return obj.id !== 'ACP';
			});
		}

		if (type == "string"){
			for (var region in regions) { 
				regionStr += regions[region].name + seperator;
			}
			return regionStr;
		} else {
			for (var region in regions) { 
				regionArr.push(regions[region].name);
			}
			return regionArr;
		}		
	}

	function removeURLParameter(url, parameter){
		var urlparts = url.split('?');
		if(urlparts.length >= 2){
			var prefix = encodeURIComponent(parameter)+'=';
			var pars = urlparts[1].split(/[&;]/g);

			for(var i = pars.length; i-- > 0;){
				if(pars[i].includes(parameter)){
					pars.splice(i, 1);
				}
			}

			url = urlparts[0]+(pars.length > 0? '?' : '')+pars.join('&');
			return url;
		}
		else{
			return url;
		}
	}

	function createTable(){
		var $table = $('.biopama_tables table');

		var $tableHeader = $('<thead><tr></tr></thead>');
		for(k in response_fields_to_table_map){
			$('tr', $tableHeader).append($(`<th>${response_fields_to_table_map[k].name}</th>`));
		}

		$table.append($tableHeader);
		$table.append($('tbody'));
		$table.append($('tfooter'));

		$table.show();

		table = $.fn.createDataTable(
			$table,
			{
				columns: retrieveTableColumns(response_fields_to_table_map),
				dom: 'Bfrtp',
				responsive: true,
				createdRow: function(row, data, dataIndex){
					$(row).on(
						'click',
						'td',
						function(e){
							if(!$(this).hasClass('selected')){
								// var wdpaId = parseInt($(this).find('td:first-child').text());
								var wdpaId = parseInt($(this).parent('tr').find('td:first-child a').text());
								
								$('tbody tr', $table).removeClass('selected');
								$(this).parent('tr').addClass('selected');

								if(!$(this).is('td:first-child')){
									if(assessmentMap){
										assessmentMap.showFeatureById(wdpaId);
									}
					
									$('html, body').animate({
										scrollTop: $("#biopama_map").offset().top - 100
									}, 1000);
								}
							}
						}
					);
				},
				rowCallback(row, data, displayNum, displayIndex, dataIndex){
					let i = 0;
					for(let k in response_fields_to_table_map){
						if(response_fields_to_table_map[k]['get_value']){
							jQuery(`td:eq(${i})`, row).html(response_fields_to_table_map[k]['get_value'](data));
						}
						i++;
					}
				}
			}
		);
	}

	function setTableData(){

		var restArguments = generateRestArgs();
		
		var url = tableDataServiceUrl+"?format=json"+restArguments;
		$.getJSON(url,function(responseData){
			table.clear().draw();

			table.rows.add(responseData);
			
			let idx = 0, isColumnVisible;
			for(k in response_fields_to_table_map){
				if(response_fields_to_table_map[k].removable){
					isColumnVisible = false;
					table.column(idx).data().each((d, i) => {
						if(d){
							isColumnVisible = true;
						}
					});
					table.column(idx).visible(isColumnVisible);
				}
				idx++;
			}

			table.draw();

			Drupal.attachBehaviors($('table.dataTable').get(0));
			
			showRowsOnMap(responseData);

			updateFilters(responseData);
		});	
	}

	// function setChartData(){
	// 	var restArguments = generateRestArgs();
	// 	if (restArguments) restArguments = ','+restArguments; //add a comma if there are filters since it will crash the REST if the comma is there without filters
	// 	var url = "/rest/gd_page_assessments_count_by_region" + restArguments;
	// 	console.log(url)
	// }
	
	function showRowsOnMap(responseData){
		if(assessmentMap){
			// Initialize the variables used to display the right layer and the right viewport on the map.
			var wdpaIds = [];
			var iso3Codes = [];
			$.each(responseData,function(idx, obj){
				var thisWdpa = parseInt(obj.wdpa_id, 10);
				if(wdpaIds.indexOf(thisWdpa) === -1) wdpaIds.push(thisWdpa); //collect all wdpa IDs
				if(iso3Codes.indexOf(obj.iso3) === -1) iso3Codes.push(obj.iso3); //collect all countries to zoom to the group
			});

			// Move the map viewport to the properly display the bbox cotaining the selected countries.
			$.getJSON(
				// getCountryBboxUrl+'?format=json&includemetadata=false&iso3codes='+iso3Codes.toString(),
				`${biopmaApiBaseUrl}/api_bbox_for_countries_dateline_safe/iso3codes=${iso3Codes.join('|')}`,
				function(responseData){
					let jsonedBbox = null;
					try{
						jsonedBbox = jQuery.parseJSON(responseData[0].api_bbox_for_countries_dateline_safe)
					}
					catch(ex){
						console.log('Service "api_bbox_for_countries_dateline_safe" returned an empty result object.');
					}
					if(jsonedBbox){
						assessmentMap.showFeaturesByIdAndBbox(
							wdpaIds,
							jsonedBbox
						);
					}
				}
			);			
		}
	}

	function initAssessmentTable(url, inputFields, map){
		tableDataServiceUrl = url,
		response_fields_to_table_map = inputFields;
		assessmentMap = map;
		setupFilters();
		createTable();
		setTableData();
		// setChartData();
		$('.assessment-table-tool-button').show();
	}

	biopamaAssessmentTable = {
		initAssessmentTable: initAssessmentTable,
		setTableData: setTableData,
		getRegions: getRegions
	}
})(jQuery);