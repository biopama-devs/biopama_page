<?php
namespace Drupal\biopama_page\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_page_assessments module.
 */
class PageController extends ControllerBase{
  public function PAGEContent() {
    $element = array(
      '#theme' => 'biopama_page_module',
    );
    return $element;
  }
  
  public function keyConceptsContent() {
    $element = array(
      '#theme' => 'page_keyconcepts_page',
      '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }

  public function assessmentToolsContent(){
    $element = array(
      '#theme' => 'page_assessmenttools_page',
      '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }

  public function assessmentsContent(){
    $element = array(
      '#theme' => 'page_assessments_page',
      '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }

  public function resourcesContent(){
    $element = array(
      '#theme' => 'page_resources_page',
      '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }

  public function managementPlansContent(){
    $element = array(
      '#theme' => 'page_managementplans_page',
      '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }
}